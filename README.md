JastAddJ-JSR308
========================

An extension to the JastAdd extensible Java compiler (JastAddJ) adding type annotations according to [JSR308](http://jcp.org/en/jsr/detail?id=308), 
which suggests extending Java with the possibility to add annotations in more locations. 
This extension covers most of the JSR but is not fully complete. 
In particular, annotations on multidimensional arrays are missing. Note also that the JSR is not finalized yet.

License & Copyright
-------------------

 * Copyright(c) 2005-2012, Torbjörn Ekman
 * Copyright(c) 2013, JastAddJ-TypeAnnotations Comitters

All rights reserved.

JastAddJ-TypeAnnotations is covered by the Modified BSD License. The full license text is distributed with this software. See the `LICENSE` file.

Building
--------

This module should be placed as a sibling directory o the JastAddJ directory. You only need to have javac and Apache Ant installed in order to build. All other tools used are available in JastAddJ.

Run ant to build the extended compiler `JavaCompiler.java`:

    $ ant

Running
-------

Usage:

    java JavaCompiler <options> <source files>
      -verbose                  Output messages about what the compiler is doing
      -classpath <path>         Specify where to find user class files
      -sourcepath <path>        Specify where to find input source files
      -bootclasspath <path>     Override location of bootstrap class files
      -extdirs <dirs>           Override location of installed extensions
      -d <directory>            Specify where to place generated class files
      -help                     Print a synopsis of standard options
      -version                  Print version information

Example:
  
    $ java JavaCompiler examples/DAG.java


